import spock.lang.Specification

/**
 * Created by jorgeromanespino on 12/02/2017.
 */
class TestMainSpec extends Specification {

    def "test main.MainGroovy"() {
        setup:
        def main = new main.MainGroovy()

        when:
        main.message = ""

        then:
        main.message == ""
    }


    def "test main.MainJava"() {
        setup:
        def main = new main.MainJava()

        when:
        main.@message = ""

        then:
        main.@message == ""
    }

}
